# Sequence Annotation Pipeline

The Sequence Annotation Pipeline enables transcript annotation via sequence
similarity (blast + Blast2GO) and/or functional prediction (InterProScan).
Annotation is performed sequentially if several databases are provided,
retaining hits from the initial searches (higher priority) and passing
unannotated sequences to searches in the next databases (lower priority).

Annotation via fasta protein databases currently requires intermediate manual
execution of Blast2GO for mapping annotations to blast results. Instructions
are provide while executing the Snakemake workflow, which will be interrupted
intentionally!

An additional KEGG annotation is possible in a similar fashion, by
integrating results of manually executed BlastKOALA (instructions on execution).
However, an appropriate number for splitting the fasta files has to be
provided with the configuration due to limits of BlastKOALA.


## Configuration and Execution

The pipeline is executed by calling Snakemake within the project folder
(working directory, configured paths are relative to here and all results and
intermediate data will be dropped here):

    snakemake -s path/to/pipeline/Snakefile --configfile snakemake.config.json --cores 4 -pk

A minimum configuration json file indicates one or more transcript fasta files
with an unique set id, a corresponding priority list of protein databases
(by id), the corresponding database files (expected to be located within
references/, indicated without suffix .fasta) and a flag whether transcripts are
novels (true: will be subjected to ORF finder) or reference (false: will be 1.
frame translated). InterPro may be included in the priority list to trigger an
InterProScan search instead of a database search.

    {
    "transcripts" : {"Sscrofa01nov" : "sscrofa_novel_trans.fasta"},
    "priorities" : {"Sscrofa01nov" : ["UPsscrofa", "UPSPmama", "UPTRmama",
                                      "UPSP", "UPTR", "InterPro"]},
    "databases" : {"UPSP"     : "uniprot_sprot",
                   "UPTR"     : "uniprot_trembl",
                   "UPsscrofa": "uniprot_proteome_AUP000008227_can+iso",
                   "UPSPmama" : "uniprot_sprot_tax40674_can+iso",
                   "UPTRmama" : "uniprot_trembl_tax40674_can+iso"},
    "novels" : {"Sscrofa01nov" : true}
    }

For more configuration options (e.g. to utilize reference GO/KEGG annotations),
please refer to
[scripts_snakemake/configuration.snakefile](scripts_snakemake/configuration.snakefile)
and if necessary other related snakefiles.

Final annotations (GO and descriptions) can be found in
`collection/{sample}/final.(annot|tsv)`, annotated proteins in
`proteins/{sample}.annotated.fasta` and KEGG annotations in
`kegg/collected/{sample}/{sample}.kegg.txt`, where {sample} is the set id given
to the transcripts in the configuration.


## Requirements

Requirements may vary with applied parameters (and thus executed Snakemake
rules), but the following lists all possibly used tools and libraries:

* Blast+
* Blast2GO
* BlastKOALA (webtool)
* faSomeRecords & faSplit (Kent tools)
* GNU R (+ libraries biomaRt, ggplot2, GO.db, KEGGREST)
* InterProScan
* Python 3
* Snakemake
* TransDecoder
* transeq (EMBOSS)

