import os
import math
import datetime
import subprocess

from snakemake.utils import R

WORKFLOW_PATH = os.path.dirname(os.path.realpath(workflow.snakefile))
EXEC_TIME = datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S')

### Configuration ###
include: WORKFLOW_PATH + "/scripts_snakemake/configuration.snakefile"

### Rules ###
include: WORKFLOW_PATH + "/scripts_snakemake/blast.snakefile"
include: WORKFLOW_PATH + "/scripts_snakemake/blast2go.snakefile"
include: WORKFLOW_PATH + "/scripts_snakemake/collection.snakefile"
include: WORKFLOW_PATH + "/scripts_snakemake/description.snakefile"
include: WORKFLOW_PATH + "/scripts_snakemake/kegg.snakefile"
include: WORKFLOW_PATH + "/scripts_snakemake/interpro.snakefile"
include: WORKFLOW_PATH + "/scripts_snakemake/preparation.snakefile"
include: WORKFLOW_PATH + "/scripts_snakemake/priority.snakefile"
include: WORKFLOW_PATH + "/scripts_snakemake/proteins.snakefile"
include: WORKFLOW_PATH + "/scripts_snakemake/reference.go.snakefile"
include: WORKFLOW_PATH + "/scripts_snakemake/reporting.versions.snakefile"
include: WORKFLOW_PATH + "/scripts_snakemake/selection.snakefile"
include: WORKFLOW_PATH + "/scripts_snakemake/translation.snakefile"
include: WORKFLOW_PATH + "/scripts_snakemake/visualization.snakefile"

rule all:
    input:
        ### GO & Descriptions ###
        # collect results
        [expand("collection/{sample}/final.annot", sample=SAMPLES)],
        [expand("collection/{sample}/final.tsv", sample=SAMPLES)],
        # provide annotated proteins
        [expand("proteins/{sample}.annotated.fasta", sample=SAMPLES)],
        # visualization
        [expand("visualization/{sample}/priority.progress.png", sample=SAMPLES)],
        ### KEGG (optional) ###
        # KEGG via BlastKOALA and Ensembl mapping
        [expand("kegg/collected/{sample}/{sample}.kegg.txt", sample=list(set(SAMPLES) & set(KOALA_PARTS.keys())))],
        ### reporting ###
        "reports/" + EXEC_TIME + ".report.txt",
        "reports/" + EXEC_TIME + ".versions.txt",


### Snakemake logging ###
onsuccess:
    print("Workflow finished, no error")
    shell("mkdir -p reports && cat {log} > reports/" + EXEC_TIME + ".snakemake.success.log")
    if MAIL_TO:
        shell(WORKFLOW_PATH + "/scripts_python/send_mail.py -f " + MAIL_FROM +
              " -s \"Annotation of project '" + PROJECT + "': terminated successfully :-)\"" +
              " -t \"\" -a reports/" + EXEC_TIME + ".snakemake.success.log " + MAIL_TO)

onerror:
    print("An error occurred")
    shell("mkdir -p reports && cat {log} > reports/" + EXEC_TIME + ".snakemake.error.log")
    if MAIL_TO:
        shell(WORKFLOW_PATH + "/scripts_python/send_mail.py -f " + MAIL_FROM +
              " -s \"Annotation of project '" + PROJECT + "': terminated unsuccessfully :-(\"" +
              " -t \"\" -a reports/" + EXEC_TIME + ".snakemake.error.log " + MAIL_TO)


### Pipeline versioning ###

# get version of snakemake workflow (aka git tag + commit hash)
# NOTE: make sure to commit latest workflow to get an updated version number!
rule pipeline_version_git:
    output:
        temp("reports/pipeline.version")
    version:
        str(subprocess.check_output("cd " + WORKFLOW_PATH +
        " && git describe --tags --long", shell=True), 'utf-8').strip()
    run:
        file = open(output[0], "w")
        file.write(version)
        file.close()

# test report rule
rule pipeline_version_report:
    input:
        "reports/pipeline.version"
    output:
        "reports/" + EXEC_TIME + ".report.txt"
    shell:
        "echo \"TA Pipeline\" > {output} && echo \"Version:\" >> {output} && " \
        "cat {input} >> {output}"
