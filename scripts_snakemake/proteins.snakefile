### Add Annotations to Proteins ###

# add descriptions to protein (transcript translations)
rule annotate_proteins:
    input:
        fasta = "translation/{proteins}.fasta",
        annot = "priority/{proteins}/complete.annot",
        descr = "description/{proteins}/blast.desc.extract.txt",
        inter = lambda wc: "interpro/" + wc.proteins + "/InterPro.supplement.txt" if wc.proteins in INTERPRO_SUP else []
    output:
        fasta = "proteins/{proteins}.annotated.fasta"
    params:
        inter = lambda wc: wc.proteins in INTERPRO_SUP
    run:
        # parse all descriptions
        trans2annot = dict()
        trans2descr = dict()
        trans2inter = dict()

        # parse annot descriptions
        with open(input.annot, 'r') as f_in:
            for line in f_in:
                splits = line.strip("\n").split("\t")
                if len(splits) > 2 and splits[2]: # is description in column 3 available?
                    trans = splits[0]
                    if trans not in trans2annot:
                        trans2annot[trans] = set()
                    trans2annot[trans].add(splits[2])

        # parse descriptions of noGO annotations
        with open(input.descr, 'r') as f_in:
            for line in f_in:
                splits = line.strip("\n").split("\t")
                trans = splits[0]
                if trans not in trans2descr:
                    trans2descr[trans] = set()
                trans2descr[trans].add(splits[1])

        # parse supplementary InterPro description
        if params.inter:
            with open(input.inter, 'r') as f_in:
                for line in f_in:
                    splits = line.strip("\n").split("\t")
                    trans = splits[0]
                    if trans not in trans2inter:
                        trans2inter[trans] = set()
                    trans2inter[trans].add(splits[1])

        # iterate fasta, select description
        with open(input.fasta, 'r') as f_in:
            with open(output.fasta, 'w') as f_out:
                for line in f_in:
                    if line.startswith(">"):
                        trans = line[1:].strip("\n")
                        # check InterPro supps first, to avoid "uncharacterized" etc.
                        if params.inter and trans in trans2inter:
                            line = ">" + trans + " " + "; ".join(trans2inter[trans]) + "\n"
                        # check no-GO Blast descriptions (only available if trans is not in annot anyway)
                        elif trans in trans2descr:
                            line = ">" + trans + " " + "; ".join(trans2descr[trans]) + "\n"
                        # finally, take description from GO annotation, if available
                        elif trans in trans2annot:
                            line = ">" + trans + " " + "; ".join(trans2annot[trans]) + "\n"
                        # else, if not annotated at all, don't alter the line
                    f_out.write(line)
