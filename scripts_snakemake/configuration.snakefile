### Required Parameters ###

# transcript sets to annotate (dict: SetID -> transcript.fasta)
TRANSCRIPTS = config["transcripts"]
SAMPLES     = TRANSCRIPTS.keys() # aka transcript sets

# annotation order use of databases (dict: SetID -> [list of DbIDs])
PRIORITIES = config["priorities"]

# available databases (dict: DbID -> protein.fasta)
DATABASES = config["databases"]

# translation mode per transcript set (dict: SetID -> true/false)
# true: novel transcripts, i.e. ORFs unknown -> ORF finder
# false: reference transcripts, i.e. ORFs as provided -> 1. frame translation
NOVELS = config["novels"]


### Optional Parameters ###

# global thread config for rules
THREADS_MAX = config.get("threads_max", 4)

# supplement insufficient descriptions with InterPro (list: SetID)
# i.e. if final descriptions are "uncharacterized protein" or "hypothetical protein"
INTERPRO_SUP = config.get("interpro_supplement", [])

# reference GOs per transcript set (dict: SetID -> reference_go.annot)
# if available, will be used as first priority annotation
GOREF = config.get("go_reference", dict())

# select representative ORF and/or transcripts
REPRESENTATIVE = config.get("representative", ["ORF"])

# indicate a number of protein fasta splits for BlastKOALA (estimate by BlastKOALA input limits!)
# if provided, manual BlastKOALA execution on splits is expected, similar to Blast2GO
KOALA_PARTS = config.get("koala_parts", dict())

# indicate a biomaRt Ensembl dataset code and a KEGG Genome code
# to query KEGG KOs for Ensembl reference transcripts via biomaRt and KEGGREST
KEGG_ENSEMBL_BM = config.get("kegg_ensembl_biomart_dataset", dict())
KEGG_ENSEMBL_KG = config.get("kegg_ensembl_kegg_genome", dict())

# project description, only used for mails so far
PROJECT = config.get("project_name", "NONAME")

# reporting, i.e. snakemake log per mail
MAIL_TO = config.get("mail_to", "")
MAIL_FROM = config["mail_from"] if MAIL_TO else ""

# apply filter for "bad" gtf entries (see preparation.snakefile for details)
GTF_FILTER = config.get("gtf_filter", True)


### Conditional Parameters ###

# number of parallel InterPro executions
INTERPRO_PARAS = config.get("interpro_parallel", 1)
# split numbers for improved InterPro parallelization (as strings with leading zeros)
SPLIT_NUMS = [str(num).rjust(len(str(INTERPRO_PARAS-1)), '0') for num in range(INTERPRO_PARAS)]

# GTF files corresponding to reference transcript sets (dict: SetID -> reference.gtf)
# (currently required for best transcript selection, for reference GO integration or KEGG annotation)
if "transcript" in REPRESENTATIVE or len(GOREF) > 0 or len(KOALA_PARTS) > 0:
    GFF_GTF = config["gff_gtf"]
    # induce renamening gff -> gff3 (via symlink)
    # induce conversion gff(3) -> gtf
    GTF = {}
    for ref, file in GFF_GTF.items():
        GFF_GTF[ref] = re.sub('\.gff3$', '.gff', file)
        GTF[ref] = re.sub('\.gff3?$', '.gtf', file)

# if reference GOs are available, use as first priority
for key in GOREF.keys():
    PRIORITIES[key].insert(0, "GOref")

# check if databases for Blast are available
# TODO: apply to check if final selection/collection should include Blast results or not
NO_BLAST = ["GOref", "InterPro"]
BLAST = [set for set in PRIORITIES if any([db not in NO_BLAST for db in PRIORITIES[set]])]


### Parameter Validation ###

# TODO: check if configuration is consistent
# i.e. sets have corresponding/required entries for each parameter

# print(TRANSCRIPTS)
# print(PRIORITIES)
# print(DATABASES)
# print(INTERPRO)
# print(SAMPLES)
# print(GOREF)
# print(GTF)
