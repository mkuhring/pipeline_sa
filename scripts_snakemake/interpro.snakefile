### InterPro - General Transcript Annotation ###

# split protein fasta for improved parallel interproscan execution:
# https://github.com/ebi-pf-team/interproscan/wiki/ImprovingPerformance
rule interproscan_01_splits:
    input:
        fasta = lambda wc: "priority/" + wc.proteins + "/InterPro.input.fasta" if wc.mode == "priority" else
                           "interpro/" + wc.proteins + "/bad_desc.fasta" # wc.mode == "supplement"
    output:
        fastas = expand("interpro/{{proteins}}/InterPro.{{mode}}.splits/split_{split}.fa", split = SPLIT_NUMS)
    params:
        splits = len(SPLIT_NUMS)
    script:
        "../scripts_python/fasta_split_N.py"

# run interproscan on proteins
# (either reference transcript 1. frame translation or novel transcripts ORFs)
# mode = priority, if part of priority iterations for non-annotated transcripts
# mode = supplement, if part of supplementation of "badly" described annotations
rule interproscan_02_execution:
    input:
        fasta = "interpro/{proteins}/InterPro.{mode}.splits/split_{split}.fa"
    output:
        tsv = "interpro/{proteins}/InterPro.{mode}.splits/split_{split}.tsv"
    params:
        out = "interpro/{proteins}/InterPro.{mode}.splits/split_{split}"
    log:
        "logs/interpro/{proteins}/InterPro.{mode}.splits/split_{split}.log"
    benchmark:
        "logs/interpro/{proteins}/InterPro.{mode}.splits/split_{split}.txt"
    threads:
        math.ceil(THREADS_MAX / INTERPRO_PARAS)
    shell:
        # remove output, since InterPro does not overwrite older files if pipeline/rule is reexecuted
        "rm -f {output} &&" \
        "interproscan.sh --input {input.fasta} --output-file-base {params.out} " \
        "--seqtype p --formats tsv,xml --iprlookup --goterms --pathways 2>&1 | tee {log}"

# merge parallel interproscan results
rule interproscan_03_merge:
    input:
        tsvs = expand("interpro/{{proteins}}/InterPro.{{mode}}.splits/split_{split}.tsv", split = SPLIT_NUMS)
    output:
        tsv = "interpro/{proteins}/InterPro.{mode}.merged.tsv"
    shell:
        "cat {input.tsvs} > {output.tsv}"

# parse interproscan results for GO annotations
# merge redundant GOs and descriptions per transcript
#
# relevant fields:
# 1.  Protein Accession
# 4.  Analysis
# 13. InterPro annotations - description
# 14. GO annotations
rule interproscan_04a_annot:
    input:
        tsv = "interpro/{proteins}/InterPro.priority.merged.tsv"
    output:
        annot = "interpro/{proteins}/InterPro.priority.annot"
    run:
        # InterPro indices
        idx_acc  = 0
        idx_ana  = 3
        idx_des = 12
        idx_go   = 13

        # collect results per transcript/protein
        results = dict()

        # parse InterPro results and merge redundant GOs+Descs
        with open(input.tsv, 'r') as f_in:
            for line in f_in:
                splits = line.strip("\n").split("\t")
                if len(splits) > 13 and splits[idx_go]:
                    acc = splits[idx_acc]
                    ana = splits[idx_ana]
                    des = splits[idx_des]

                    if acc not in results:
                        results[acc] = dict()

                    for go in splits[idx_go].split("|"):
                        go_des = go + "\t" + des
                        if go_des not in results[acc]:
                            results[acc][go_des] = set()
                        results[acc][go_des].add(ana)

        # export annot
        with open(output.annot, 'w') as f_out:
            for acc in sorted(results.keys()):
                for go_des, ana in results[acc].items():
                    annot = acc + "\t" + go_des + " [InterPro - " + ','.join(sorted(ana)) + "]"
                    f_out.write(annot + "\n")


### InterPro - Bad Annotation Supplementation ###

# identify transcript translations with annotation (with or without GO) but uninformative description
# i.e.: "uncharacterized protein" (UniProt), "hypothetical protein" (NCBI)
rule bad_descs_annotations:
    input:
        annot = "priority/{proteins}/complete.annot",
        descr = "description/{proteins}/blast.desc.extract.txt"
    output:
        bad = "interpro/{proteins}/bad_desc.txt"
    run:
        bad_descs = ["uncharacterized protein",
                     "hypothetical protein"]

        with open(output.bad, 'w') as f_out:
            with open(input.annot, 'r') as f_in:
                for line in f_in:
                    splits = line.strip("\n").split("\t")
                    if len(splits) > 2:
                        if any([bad in splits[2].lower() for bad in bad_descs]):
                            f_out.write(splits[0] + "\n")
            with open(input.descr, 'r') as f_in:
                for line in f_in:
                    splits = line.strip("\n").split("\t")
                    if any([bad in splits[1].lower() for bad in bad_descs]):
                        f_out.write(splits[0] + "\n")

# extract sequences of "badly" descripted transcripts
rule bad_descs_sequences:
    input:
        ids = "interpro/{proteins}/bad_desc.txt",
        fasta = "translation/{proteins}.fasta"
    output:
        fasta = "interpro/{proteins}/bad_desc.fasta"
    shell:
        "faSomeRecords {input.fasta} {input.ids} {output.fasta}"

# parse interproscan results for descriptions
# merge redundant descriptions per transcript
#
# relevant fields:
# 1.  Protein Accession
# 4.  Analysis
# 13. InterPro annotations - description
rule interproscan_04b_description:
    input:
        tsv = "interpro/{proteins}/InterPro.supplement.merged.tsv"
    output:
        txt = "interpro/{proteins}/InterPro.supplement.txt"
    run:
        # InterPro indices
        idx_acc  = 0
        idx_ana  = 3
        idx_des = 12

        # collect results per transcript/protein
        results = dict()

        # parse InterPro results and merge redundant description
        with open(input.tsv, 'r') as f_in:
            for line in f_in:
                splits = line.strip("\n").split("\t")
                if len(splits) >= 13:
                    acc = splits[idx_acc]
                    ana = splits[idx_ana]
                    des = splits[idx_des]

                    if acc not in results:
                        results[acc] = dict()

                    if des not in results[acc]:
                        results[acc][des] = set()
                    results[acc][des].add(ana)

        # export annot
        with open(output.txt, 'w') as f_out:
            for acc in sorted(results.keys()):
                for des, ana in results[acc].items():
                    annot = acc + "\t" + des + " [InterPro - " + ','.join(sorted(ana)) + "]"
                    f_out.write(annot + "\n")
