### Collection Selection Criteria ###

# get sequence lengths
# (based on: http://www.danielecook.com/generate-fasta-sequence-lengths/)
rule sequence_lengths:
    input:
        fasta = "translation/{proteins}.fasta"
    output:
        txt = "selection/{proteins}/sequence.lengths.txt"
    shell:
        "cat {input.fasta} | awk '$0 ~ \">\" {{print c; c=0;printf substr($0,2,100) \"\t\"; }} $0 !~ \">\" {{c+=length($0);}} END {{ print c; }}' > {output.txt}"

# extract best blast evalue for annotated sequences
rule blast_best_evalue_extract:
    input:
        blast = "blast/{proteins}/{db}.hits.xml",
        hits = "priority/{proteins}/{db}.found.txt"
    output:
        best = "selection/{proteins}/blast.{db}.best.txt"
    run:
        # annotated hits
        hits = set()
        with open(input.hits, 'r') as f_in:
            for line in f_in:
                hits.add(line.strip("\n").strip())

        # collect best e-value per query
        best_hits = dict()

        # simply parse all Hsp_evalue per Iteration (aka query sequence) from the Blast xml
        # (xml parsing based on http://stackoverflow.com/a/1912516)
        from xml.dom import minidom
        xmldoc = minidom.parse(input.blast)
        queries = xmldoc.getElementsByTagName('Iteration')

        for q in queries:
            iter_def = q.getElementsByTagName('Iteration_query-def')[0].childNodes[0].nodeValue
            if iter_def in hits:
                evalue_list = q.getElementsByTagName('Hsp_evalue')
                if evalue_list:
                    # find best evalue
                    if iter_def not in best_hits:
                        best_hits[iter_def] = float(1)
                    for e in evalue_list:
                        hit_evalue = float(e.childNodes[0].nodeValue)
                        if hit_evalue < best_hits[iter_def]:
                            best_hits[iter_def] = hit_evalue

        # export best evalue per annotated hit
        with open(output.best, 'w') as f_out:
            for id, evalue in best_hits.items():
                f_out.write(id + "\t" + str(evalue) + "\n")

# merge all best evalues for all blast-based priorities
rule blast_best_evalue_merge:
    input:
        evalues = lambda wc: expand("selection/{proteins}/blast.{db}.best.txt", proteins =  wc.proteins,
                                    db = [prio for prio in PRIORITIES[wc.proteins] if prio not in ["GOref", "InterPro"]])
    output:
        merged = "selection/{proteins}/blast.best.merged.txt"
    shell:
        "cat {input.evalues} > {output.merged}"


# count contributing InterPro tools per sequence
# TODO: use only if InterPro is in PRIORITIES
rule interpro_tool_counts:
    input:
        annot = "priority/{proteins}/InterPro_hits.annot"
    output:
        counts = "selection/{proteins}/interpro.tool.counts.txt"
    run:
        id2tools = dict();

        # collect unique tools per sequence
        with open(input.annot, 'r') as f_in:
            for line in f_in:
                splits = line.strip("\n").split("\t")
                id = splits[0]
                desc = splits[2]
                tools = desc.split("[InterPro - ")[-1][:-1].split(",")
                if id not in id2tools:
                    id2tools[id] = set()
                id2tools[id].update(tools)

        # export tool counts
        with open(output.counts, 'w') as f_out:
            for id, tools in id2tools.items():
                f_out.write(id + "\t" + str(len(tools)) + "\n")


### Select Representative ORFs and Transcripts ###

# collect final transcripts IDs, either all or ORF/transcript representatives
def final_ids(wc):
    if "transcript" in REPRESENTATIVE:
        file = "selection/" + wc.proteins + "/best.trans.txt"
    elif "ORF" in REPRESENTATIVE:
        file = "selection/" + wc.proteins + "/best.orfs.txt" if NOVELS[wc.proteins] else "selection/" + wc.proteins + "/best.all.txt"
    else: # no selection/reduction
        file = "selection/" + wc.proteins + "/best.all.txt"
    return(file)

rule select_final:
    input:
        ids = final_ids
    output:
        ids = "selection/{proteins}/final.transcripts.txt"
    shell:
        "cp {input.ids} {output.ids}"

# get simple list of all annotated transcripts
rule select_all:
    input:
        go = "priority/{proteins}/complete.annot",
        nogo = "description/{proteins}/blast.desc.extract.txt"
    output:
        "selection/{proteins}/best.all.txt"
    shell:
        "cat {input} | cut -f1 | sort | uniq > {output}"

# select one representative ORF per transcript
rule select_orf:
    input:
        blast = "selection/{proteins}/blast.best.merged.txt",
        length = "selection/{proteins}/sequence.lengths.txt",
        interpro = "selection/{proteins}/interpro.tool.counts.txt",
        blast_nogo = "description/{proteins}/blast.desc.evalues.txt"
    output:
        best = "selection/{proteins}/best.orfs.txt"
    run:
        R('''
            input.blast = "{input.blast}"
            input.length = "{input.length}"
            input.interpro = "{input.interpro}"
            input.blastnogo = "{input.blast_nogo}"

            # import data
            table.blast <- read.table(input.blast, sep = "\\t",
                                      col.names = c("ORF", "Value"),
                                      colClasses = c("character", "numeric"))
            table.interpro <- read.table(input.interpro, sep = "\\t",
                                         col.names = c("ORF", "Value"),
                                         colClasses = c("character", "integer"))
            table.length <- read.table(input.length, sep = "\\t",
                                         col.names = c("ORF", "Length"),
                                         colClasses = c("character", "integer"))
            table.blastnogo <- read.table(input.blastnogo, sep = "\\t",
                                      col.names = c("ORF", "Value"),
                                      colClasses = c("character", "numeric"))

            # define criteria priority (Blast GO hit -> InterPro -> Blast no-GO hit)
            table.blast$Priority <- "1"
            table.interpro$Priority <- "2"
            table.blastnogo$Priority <- "3"

            # unify criteria values sorting direction such that smaller values are better
            # (evalue, 1/#interpro)
            table.interpro$Value <- 1 / table.interpro$Value

            # merge criteria
            table.criteria <- rbind(table.blast, table.interpro, table.blastnogo)
            table.criteria <- merge(table.criteria, table.length, by = "ORF", all.x = TRUE)

            # extract original sequence ids
            table.criteria$OrgID <- sub("_ORF\\\\d+$", "", table.criteria$ORF)

            print(head(table.criteria))

            # group/sort by original sequence ID, then Priority (1 - Blast, 2 - InterPro, then 3 - BlastNoGO),
            # then by Values (small values first) and finale by Length (bigger first)
            sort.idx <- order(table.criteria$OrgID, table.criteria$Priority, table.criteria$Value, -table.criteria$Length)
            table.criteria <- table.criteria[sort.idx, ]

            # select best ORF, i.e. first occuring ORF per original ID with:
            # highest priority, smallest possible value and (in case of ties) largest length
            orf.selection <- which(!duplicated(table.criteria$OrgID))
            orf.best <- table.criteria$ORF[orf.selection]

            # export best ORFs
            file.best.orfs = "{output.best}"
            writeLines(orf.best, file.best.orfs)

            # # print value ties
            # dups <- which(duplicated(table.criteria[c("Value", "Priority", "OrgID")]))
            # table.dups <- table.criteria[unique(c(rbind(dups-1, dups))), ]
            # print(table.dups)
        ''')

# select one representative transcript per gene
rule select_transcript:
    input:
        hits = lambda wc: "selection/" + wc.proteins + "/best.orfs.txt" \
                            if NOVELS[wc.proteins] and "ORF" in REPRESENTATIVE \
                            else "selection/" + wc.proteins + "/best.all.txt",
        ids = lambda wc: GTF[wc.proteins] + ".ids.clean",
        blast = "selection/{proteins}/blast.best.merged.txt",
        length = "selection/{proteins}/sequence.lengths.txt",
        interpro = "selection/{proteins}/interpro.tool.counts.txt",
        blast_nogo = "description/{proteins}/blast.desc.evalues.txt"
    output:
        best = "selection/{proteins}/best.trans.txt"
    run:
        R('''
            input.hits = "{input.hits}"
            input.ids = "{input.ids}"

            input.blast = "{input.blast}"
            input.length = "{input.length}"
            input.interpro = "{input.interpro}"
            input.blastnogo = "{input.blast_nogo}"

            # import data
            table.hits <- read.table(input.hits, sep = "\\t",
                                     col.names = c("Transcript"),
                                     colClasses = c("character"))
            table.ids <- read.table(input.ids, sep = "\\t",
                                     col.names = c("Transcript", "Gene", "Gene.Name"),
                                     colClasses = c("character", "character", "character"))
            table.ids$Gene.Name <- NULL

            table.blast <- read.table(input.blast, sep = "\\t",
                                      col.names = c("Transcript", "Value"),
                                      colClasses = c("character", "numeric"))
            table.interpro <- read.table(input.interpro, sep = "\\t",
                                         col.names = c("Transcript", "Value"),
                                         colClasses = c("character", "integer"))
            table.length <- read.table(input.length, sep = "\\t",
                                         col.names = c("Transcript", "Length"),
                                         colClasses = c("character", "integer"))
            table.blastnogo <- read.table(input.blastnogo, sep = "\\t",
                                      col.names = c("Transcript", "Value"),
                                      colClasses = c("character", "numeric"))

            # define criteria priority (Blast -> InterPro)
            table.blast$Priority <- "1"
            table.interpro$Priority <- "2"
            table.blastnogo$Priority <- "3"

            # unify criteria values sorting direction such that smaller values are better
            # (evalue, 1/#interpro)
            table.interpro$Value <- 1 / table.interpro$Value

            # clean ORF suffixes to enable transcript to gene mapping
            table.hits$Transcript.Clean <- sub("_ORF\\\\d+$", "", table.hits$Transcript)

            # merge criteria and ids
            table.ids.sub <- merge(table.hits, table.ids, by.x = "Transcript.Clean", by.y = "Transcript", all.x = TRUE)

            table.criteria <- rbind(table.blast, table.interpro, table.blastnogo)
            table.criteria <- merge(table.ids.sub, table.criteria, by = "Transcript", all.x = TRUE)
            table.criteria <- merge(table.criteria, table.length, by = "Transcript", all.x = TRUE)

            # group/sort by original Gene ID, then Priority (1 - Blast, 2 - InterPro, then 3 - BlastNoGO),
            # then by Values (small values first) and finale by Length (bigger first)
            sort.idx <- order(table.criteria$Gene, table.criteria$Priority, table.criteria$Value, -table.criteria$Length)
            table.criteria <- table.criteria[sort.idx, ]

            # select best Transcript, i.e. first occuring Transcript per original ID with:
            # highest priority, smallest possible value and (in case of ties) largest length
            trans.selection <- which(!duplicated(table.criteria$Gene))
            trans.best <- table.criteria$Transcript[trans.selection]

            # NOTE: transcripts in hit table which are not represented by Blast or InterPro
            # are assumend to be hits via reference GO mapping. Thus, Value and Priority
            # will be NA now and sorting/selection is achieved via transcript length only.

            # export best Transcripts
            file.best.trans = "{output.best}"
            writeLines(trans.best, file.best.trans)
        ''')
