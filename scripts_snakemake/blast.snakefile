### Blast against UniProt & NCBI ###

# create blast database from fasta
rule blast_database:
    input:
        fasta = "references/{db}.fasta"
    output:
        tag = "references/blastdb_{db}/{db}.tag"
    params:
        out = "references/blastdb_{db}/{db}"
    log:
        "logs/blast/{db}.log"
    shell:
        "makeblastdb -in {input.fasta} -out {params.out} -dbtype prot -parse_seqids -logfile {log} && touch {output.tag}"

# blast annotation of DEG transcripts translations
# parameter for Blast2GO: https://www.blast2go.com/support/blog/22-blast2goblog/111-format-fasta-file-blast
rule blast_proteins:
    input:
        fasta = lambda wc: "priority/" + wc.proteins + "/" + wc.db + ".input.fasta",
        db = lambda wc: "/home/kuhringm/ncbi/blast/db/nr/nr.pal" if wc.db == "ncbi_nr" else "references/blastdb_" + DATABASES[wc.db] + "/" + DATABASES[wc.db] + ".tag"
    output:
        "blast/{proteins}/{db}.hits.xml"
    params:
        all = "-outfmt 5 -evalue 1e-3 -word_size 4 -show_gis -max_target_seqs 20 -max_hsps 20",
        db = lambda wc: "/home/kuhringm/ncbi/blast/db/nr/nr" if wc.db == "ncbi_nr" else "references/blastdb_" + DATABASES[wc.db] + "/" + DATABASES[wc.db]
    log:
        "logs/blast/{proteins}/{db}.hits.log"
    benchmark:
        "logs/blast/{proteins}/{db}.hits.txt"
    threads:
        THREADS_MAX
    shell:
        "blastp -num_threads {threads} {params.all} -db {params.db} -query {input.fasta} -out {output} 2>&1 | tee {log}"

# create blast database from NR with a GI filter
rule blast_database_alias:
    input:
        gis = "references/ncbi_protein_tax{taxid}.gi",
        db = "/home/kuhringm/ncbi/blast/db/nr/nr.pal"
    output:
        db = "references/blastdb_nr_tax{taxid,\d+}/nr_tax{taxid}.phr"
    params:
        db_in = "/home/kuhringm/ncbi/blast/db/nr/nr",
        db_out = "references/blastdb_nr_tax{taxid}/nr_tax{taxid}",
        title = "nr_tax{taxid}"
    shell:
        "blastdb_aliastool -gilist {input.gis} -db {params.db_in} -out {params.db_out} -title {params.title} -dbtype prot"
