### Prepare Reference GOs (optional) ###

# assign gene references GOs to reference transcripts
# reference gene GO annotations should look as follow (tab separated):
# Ensembl_Gene_ID	GO_Term_Accession	Description
# e.g. Ensembl Mart export:
# ENSSSCG00000000120	GO:0005515	ankyrin repeat domain 54 [Source:HGNC Symbol;Acc:HGNC:25185]
rule ref_GO_gene_to_transcript:
    input:
        gos = lambda wc: GOREF[wc.proteins],
        ids = lambda wc: GTF[wc.proteins] + ".ids.clean"
    output:
        annot = "priority/{proteins}/GOref.transcript.annot"
    run:
        gene2go = dict()
        with open(input.gos, 'r') as f:
            for line in f:
                splits = line.strip("\n").split("\t")
                gene_id = splits[0]
                go_id = splits[1]
                desc = splits[2]
                if go_id:
                    if gene_id not in gene2go:
                        gene2go[gene_id] = []
                    gene2go[gene_id].append(go_id + "\t" + desc)
        with open(input.ids, 'r') as f_in:
            with open(output.annot, 'w') as f_out:
                for line in f_in:
                    splits = line.strip("\n").split("\t")
                    trans_id = splits[0]
                    gene_id = splits[1]
                    if gene_id in gene2go:
                        for gos in gene2go[gene_id]:
                            f_out.write(trans_id + "\t" + gos + "\n")

# map reference GO to input transcripts
rule ref_GO_map_transcripts:
    input:
        annot = "priority/{proteins}/GOref.transcript.annot",
        fasta = "translation/{proteins}.fasta"
    output:
        annot = "priority/{proteins}/GOref.mapped.annot"
    run:
        # parse input transcript
        t_input = set()
        t_input.update([line.strip("\n")[1:] for line in open(input.fasta, 'r') if line.startswith(">")])

        # iterate annot and only export matching transcripts
        with open(input.annot, 'r') as f_in:
            with open(output.annot, 'w') as f_out:
                for line in f_in:
                    if line.split("\t")[0] in t_input:
                        f_out.write(line)
