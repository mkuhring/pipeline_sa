### Mapping Ensembl to KEGG Orthology ###

# get KOs for Ensemble reference transcripts
# via biomaRt (ensembl > uniprot) and KEGGREST (uniprot > ssc > ko)
rule kegg_via_ensembl:
    output:
        kegg = "kegg/via_ensembl/{sample}/ko.txt"
    params:
        ds_biomart = lambda wc: KEGG_ENSEMBL_BM[wc.sample],
        ds_kegg =  lambda wc: KEGG_ENSEMBL_KG[wc.sample]
    run:
        R(r'''
        # http://www.ensembl.org/info/data/biomart/biomart_r_package.html/
        library(biomaRt)
        ensembl = useMart(biomart="ensembl", dataset="{params.ds_biomart}")
        mapping <- getBM(mart=ensembl, attributes=c('ensembl_transcript_id','uniprotswissprot','uniprotsptrembl'))

        # http://bioconductor.org/packages/release//bioc/vignettes/KEGGREST/inst/doc/KEGGREST-vignette.html
        library(KEGGREST)
        up2org <- keggConv("{params.ds_kegg}", "uniprot")
        mapping$kegg.genes1 <- up2org[paste0("up:", mapping$uniprotswissprot)]
        mapping$kegg.genes2 <- up2org[paste0("up:", mapping$uniprotsptrembl)]
        idx = which(!is.na(mapping$kegg.genes1))
        mapping$KEGG.Genes <- replace(x = mapping$kegg.genes2, list = idx, mapping$kegg.genes1[idx])
        org2ko <- keggLink("ko", "{params.ds_kegg}")
        mapping$KEGG.KO <- sub("ko:", "", org2ko[mapping$KEGG.Genes])

        names(mapping)[1] <- "Transcript.ID"
        write.table(mapping[ ,c("Transcript.ID", "KEGG.KO")], "{output.kegg}", na = "",
                    quote = F, sep = "\t", row.names = F, col.names = T)
        ''')


### KEGG Orthology Mapping via BlastKOALA ###

# split protein fasta
# (using faSplit from Kent tools: http://hgdownload.cse.ucsc.edu/admin/exe/linux.x86_64/)
rule kegg_blastkoala_fasta_splits:
    input:
        fasta = "translation/{sample}.fasta"
    output:
        fastas = "kegg/blastkoala/{sample}/",
        tag = "kegg/blastkoala/{sample}/tag.parts.txt"
    params:
        parts = lambda wc: KOALA_PARTS[wc.sample],
        prefix = "kegg/blastkoala/{sample}/{sample}_"
    shell:
        "faSplit sequence {input.fasta} {params.parts} {params.prefix} && touch {output.tag}"

# count number of proteins in unsplitted fasta
rule kegg_blastkoala_fasta_counts_full:
    input:
        fasta = "translation/{sample}.fasta"
    output:
        count = "translation/{sample}.count"
    shell:
        "grep \"^>\" {input.fasta} | wc -l > {output.count}"

# count number of proteins in splitted fasta
rule kegg_blastkoala_fasta_counts_part:
    input:
        tag = "kegg/blastkoala/{sample}/tag.parts.txt"
    output:
        count = "kegg/blastkoala/{sample}/{sample}_{part}.count"
    params:
        fasta = "kegg/blastkoala/{sample}/{sample}_{part}.fa"
    shell:
        "grep \"^>\" {params.fasta} | wc -l > {output.count}"

# dummy rule for manual BlastKOALA execution
#
# it will break workflow execution since it won't produce the output files
# execute BlastKOALA manually as suggested and continue the workflow
rule kegg_blastkoala_manually:
    input:
        tag = "kegg/blastkoala/{sample}/tag.parts.txt",
        count_p = "kegg/blastkoala/{sample}/{sample}_{part}.count",
        count_f = "translation/{sample}.count"
    output:
        kegg1 = "kegg/blastkoala/{sample}/{sample}_{part}/user_ko.txt",
        kegg2 = "kegg/blastkoala/{sample}/{sample}_{part}/user_ko_definition.txt"
    params:
        split = "kegg/blastkoala/{sample}/{sample}_{part}.fa"
    run:
        with open(input.count_p, 'r') as f_in:
            count_p = f_in.readline().rstrip()
        with open(input.count_f, 'r') as f_in:
            count_f = f_in.readline().rstrip()

        message = '''
### BlastKOALA ###

run the following steps manually in BlastKOALA (http://www.kegg.jp/blastkoala/)
1. Upload query amino acid sequences in FASTA format:
   ''' + params.split + " (" + count_p + " of " + count_f + ''')
2. Enter taxonomy group of your genome
   * set taxid if known or select corresponding kingdom
3. Enter KEGG GENES database file to be searched
   * note: select # of parts based on target database limits
4. confirm job request mail (Submit)
5. export results as text file:
   ''' + output.kegg1 + '''
   ''' + output.kegg2 + '''
   * follow link in "Job completed" mail, then...
   * Annotation data -> Download
   * Annotation data -> View -> Annotation data -> Download detail
6. optional: make a backup in case of snakemake hickups
'''
        print(message)

# merge results of BlastKOALA splits
rule kegg_blastkoala_split_merge:
    input:
        kegg1 = lambda wc: ["kegg/blastkoala/" + wc.sample + "/" + wc.sample + "_" + str(part)
                            + "/user_ko.txt" for part in range(KOALA_PARTS[wc.sample])],
        kegg2 = lambda wc: ["kegg/blastkoala/" + wc.sample + "/" + wc.sample + "_" + str(part)
                            + "/user_ko_definition.txt" for part in range(KOALA_PARTS[wc.sample])],
    output:
        kegg1 = "kegg/blastkoala/{sample}/{sample}_user_ko.txt",
        kegg2 = "kegg/blastkoala/{sample}/{sample}_user_ko_definition.txt"
    shell:
        '''
        cat {input.kegg1} > {output.kegg1}
        cat {input.kegg2} > {output.kegg2}
        '''

# collect KEGG KOs from BlastKOALA results
# primary hit if available, otherwise secondary hit
rule kegg_blastkoala_collect:
    input:
        kegg = "kegg/blastkoala/{sample}/{sample}_user_ko_definition.txt"
    output:
        kegg = "kegg/blastkoala/{sample}/ko.txt"
    run:
        with open(input.kegg, 'r') as f_in:
            with open(output.kegg, 'w') as f_out:
                f_out.write("\t".join(["Transcript.ID", "KEGG.KO"]) + "\n")
                trans_id = ""
                for line in f_in:
                    splits = line.strip("\n").split("\t")
                    if splits[1]: ko = splits[1] # primary hit
                    elif splits[4]: ko = splits[4] # secondary hit
                    else: ko = ""
                    if splits[0]: trans_id = splits[0] # get trans id if available, else use last one
                    for k in ko.split("/"):
                        f_out.write("\t".join([trans_id, k]) + "\n")


### Finalize KEGG Annotation ###

# merge Ensembl-based KOs (1. priority) and BlastKOALA KOs (2. priority)
rule kegg_merge:
    input:
        koala = "kegg/blastkoala/{sample}/ko.txt",
        ensembl = "kegg/via_ensembl/{sample}/ko.txt"
    output:
        kegg = "kegg/collected/{sample}/ko.merged.txt"
    run:
        trans2ko = dict()
        # parse all BlastKOALA transcripts, with or without KO
        with open(input.koala, 'r') as f_in:
            next(f_in)
            for line in f_in:
                splits = line.strip("\n").split("\t")
                if splits[0] not in trans2ko:
                    trans2ko[splits[0]] = set()
                trans2ko[splits[0]].add(splits[1])
        # replace BlastKOALA KO if Ensembl-based KO ist available
        with open(input.ensembl, 'r') as f_in:
            next(f_in)
            for line in f_in:
                splits = line.strip("\n").split("\t")
                if splits[1]:
                    trans2ko[splits[0]] = {splits[1]}
        # export merged KOs
        with open(output.kegg, 'w') as f_out:
            f_out.write("\t".join(["Transcript.ID", "KEGG.KO"]) + "\n")
            for trans in sorted(trans2ko.keys()):
                for ko in sorted(trans2ko[trans]):
                    f_out.write("\t".join([trans, ko]) + "\n")

# control KO merging and add gene ids
rule kegg_collect:
    input:
        kegg = lambda wc: "kegg/collected/" + wc.sample + "/ko.merged.txt" if wc.sample in KEGG_ENSEMBL_BM \
                     else "kegg/blastkoala/" + wc.sample + "/ko.txt",
        ids = lambda wc: GTF[wc.sample] + ".ids.clean"
    output:
        kegg = "kegg/collected/{sample}/ko.ids.txt"
    run:
        # ORF regex
        import re
        orf_regex = re.compile("_ORF\d+$")

        trans2gene = dict()
        with open(input.ids, 'r') as f_in:
            for line in f_in:
                splits = line.strip("\n").split("\t")
                trans2gene[splits[0]] = splits[1]
        with open(input.kegg, 'r') as f_in:
            with open(output.kegg, 'w') as f_out:
                f_out.write("Gene.ID" + "\t" + f_in.readline())
                for line in f_in:
                    splits = line.strip("\n").split("\t")
                    trans_id = orf_regex.sub("",splits[0])
                    f_out.write(trans2gene[trans_id] + "\t" + line)

# map KEGG Orthology to KEGG Pathways
rule kegg_get_pathways:
    input:
        kegg = "kegg/collected/{sample}/ko.ids.txt"
    output:
        kegg = "kegg/collected/{sample}/{sample}.kegg.txt"
    run:
        R(r'''
            library(KEGGREST)
            ko2path <- keggLink("pathway", "ko") # KO to path mapping
            paths <- keggList("pathway")         # path descriptions
            kos <- keggList("ko")                # KO descriptions

            mapping <- read.table("{input.kegg}", header = TRUE, sep = "\t", stringsAsFactor = FALSE)
            mapping$KEGG.Pathway <- sub("path:", "", ko2path[paste0("ko:", mapping$KEGG.KO)])
            mapping$KEGG.KO.Desc <- kos[paste0("ko:", mapping$KEGG.KO)]
            mapping$KEGG.Pathway.Desc <- paths[paste0("path:", mapping$KEGG.Pathway)]

            write.table(mapping, "{output.kegg}", na = "", quote = F, sep = "\t", row.names = F, col.names = T)
        ''')
