### Collect results ###

# collect GO annot acccording to selected transcripts
rule collect_annot:
    input:
        annot = "priority/{sample}/complete.annot",
        ids = "selection/{sample}/final.transcripts.txt"
    output:
        annot = "collection/{sample}/selection.annot"
    run:
        # parse selected transcript
        ids = set([line.strip("\n") for line in open(input.ids, 'r')])
        # iterate annot and only export for selected transcripts
        with open(input.annot, 'r') as f_in:
            with open(output.annot, 'w') as f_out:
                for line in f_in:
                    if line.split("\t")[0] in ids:
                        f_out.write(line)

# collect NoGO descriptions acccording to selected transcripts
rule collect_descr:
    input:
        descr = "description/{sample}/blast.desc.extract.txt",
        ids = "selection/{sample}/final.transcripts.txt"
    output:
        descr = "collection/{sample}/selection.descr"
    run:
        # parse selected transcript
        ids = set([line.strip("\n") for line in open(input.ids, 'r')])
        # iterate descriptions and only export for selected transcripts
        with open(input.descr, 'r') as f_in:
            with open(output.descr, 'w') as f_out:
                for line in f_in:
                    if line.split("\t")[0] in ids:
                        f_out.write(line)

# add GO descriptions to annot
rule collect_annot_go_terms:
    input:
        annot = "collection/{sample}/selection.annot"
    output:
        annot = "collection/{sample}/goterms.annot"
    run:
        R('''
        if (!require(GO.db)){{
          source("https://bioconductor.org/biocLite.R")
          biocLite("GO.db")
          require(GO.db)
        }}

        go.terms <- Term(GOTERM)
        go.table <- data.frame(GO.ID = names(go.terms), GO.Term = go.terms)

        annot.file <- "{input.annot}"
        annot.table <- read.table(annot.file, sep = "\t", stringsAsFactors = FALSE,  fill = TRUE,
                                  col.names = c("Sequence.ID", "GO.ID", "Description"), quote = "")
        annot.table$Order <- 1:nrow(annot.table)

        merge.table <- merge(annot.table, go.table, by="GO.ID", all.x = TRUE)
        merge.table <- merge.table[order(merge.table$Order),]
        merge.table <- merge.table[, c("Sequence.ID", "GO.ID", "Description", "GO.Term")]

        annot.out <- "{output.annot}"
        write.table(merge.table, annot.out, sep = "\t", quote = FALSE,
                    row.names = FALSE, col.names = FALSE)
        ''')

# remove ORF suffices from final annot
# optionally: add gene ids, if representative transcripts were selected
rule collect_annot_clean:
    input:
        annot = "collection/{sample}/selection.annot",
        gene = lambda wc: GTF[wc.sample] + ".ids.clean" if "transcript" in REPRESENTATIVE else []
    output:
        annot = "collection/{sample}/final.annot"
    params:
        gene = lambda wc: "transcript" in REPRESENTATIVE
    run:
        # ORF regex
        import re
        orf_regex = re.compile("_ORF\d+$")
        # parse gene ids
        trans2gene = dict()
        if params.gene:
            with open(input.gene, 'r') as f_in:
                    for line in f_in:
                        splits = line.strip("\n").split("\t")
                        trans2gene[splits[0]] = splits[1]
        # iterate annot
        with open(input.annot, 'r') as f_in:
            with open(output.annot, 'w') as f_out:
                for line in f_in:
                    splits = line.strip("\n").split("\t")
                    # remove ORF suffix, if present
                    seq_id = orf_regex.sub("",splits[0])
                    # replace transcript with gene id, if representative was selected
                    if params.gene:
                        seq_id = trans2gene[seq_id]
                    splits[0] = seq_id
                    # add empty split if no description is present (for complete tab separation)
                    if len(splits) == 2:
                        splits.append("")
                    f_out.write("\t".join(splits) + "\n")


# collapse annot (the one with descriptions) into tabular format, one row per transcript
# optionally: add gene ids, if representative transcripts were selected
# optionally: collapse and add supplementary InterPro annots
rule collect_annot_to_tsv:
    input:
        annot = "collection/{sample}/goterms.annot",
        inter = lambda wc: "interpro/" + wc.sample + "/InterPro.supplement.txt" if wc.sample in INTERPRO_SUP else [],
        gene = lambda wc: GTF[wc.sample] + ".ids.clean" if "transcript" in REPRESENTATIVE else [],
        descr = "collection/{sample}/selection.descr"
    output:
        tsv = "collection/{sample}/final.tsv"
    params:
        inter = lambda wc: wc.sample in INTERPRO_SUP,
        gene = lambda wc: "transcript" in REPRESENTATIVE
    run:
        trans2go = dict()
        trans2desc = dict()
        trans2term = dict()
        trans2gene = dict()
        trans2desc_ip = dict()

        # parse annot
        with open(input.annot, 'r') as f_in:
            for line in f_in:
                splits = line.strip("\n").split("\t")
                trans = splits[0]
                if trans not in trans2go:
                    trans2desc[trans] = set()
                    trans2go[trans] = list()
                    trans2term[trans] = list()
                    trans2desc_ip[trans] = set()
                trans2go[trans].append(splits[1])
                if len(splits) > 2 and splits[2]:
                    trans2desc[trans].add(splits[2])
                if splits[3]:
                    trans2term[trans].append(splits[3])

        # parse descriptions of noGO annotations
        with open(input.descr, 'r') as f_in:
            for line in f_in:
                splits = line.strip("\n").split("\t")
                trans = splits[0]
                if trans not in trans2go:
                    trans2desc[trans] = set()
                    trans2go[trans] = list()
                    trans2term[trans] = list()
                    trans2desc_ip[trans] = set()
                trans2desc[trans].add(splits[1])

        # parse gene ids
        if params.gene:
            with open(input.gene, 'r') as f_in:
                    for line in f_in:
                        splits = line.strip("\n").split("\t")
                        trans = splits[0]
                        gene = splits[1]
                        trans2gene[trans] = gene

        # parse supplementary InterPro description
        if params.inter:
            with open(input.inter, 'r') as f_in:
                for line in f_in:
                    splits = line.strip("\n").split("\t")
                    trans = splits[0]
                    if trans in trans2desc_ip: # check because main annot/descr maybe selected transcripts only
                        trans2desc_ip[trans].add(splits[1])

        import re
        orf_regex = re.compile("_ORF\d+$") # remove ORF suffix for gene matching
        with open(output.tsv, 'w') as f_out:
            header_g = "Gene.ID" + "\t" if params.gene else ""
            header = "Transcript.ID" + "\t" + "Sequence.Description" + "\t" + "Annotation.GO.ID" + "\t" + "Annotation.GO.Term"
            header_ip = "\t" + "Supp.InterPro.Description" if params.inter else ""
            f_out.write(header_g + header + header_ip + "\n")
            for trans in sorted(trans2go.keys()):
                trans_c = orf_regex.sub("",trans)
                line_g = trans2gene[trans_c] + "\t" if params.gene else ""
                line = trans_c + "\t" + ";".join(trans2desc[trans]) + "\t" + ",".join(trans2go[trans]) + "\t" + ";".join(trans2term[trans])
                line_ip = "\t" + ";".join(trans2desc_ip[trans]) if params.inter else ""
                f_out.write(line_g + line + line_ip + "\n")
