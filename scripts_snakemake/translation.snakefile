import re


### Translate Reference Transcripts ###

# clean fasta header, i.e.
# * keep id only (cut of after "|" or " ")
# * remove transcript version (ENSEMBLE)
# * replace ":" in ids by "_"
# >ENSSSCT00000033736.1 cds:known chromosome:Sscrofa10.2:14:52424836:52424872:-1 gene:ENSSSCG00000030890.1 gene_biotype:IG_J_gene transcript_biotype:IG_J_gene gene_symbol:IGLJ
# -> >ENSSSCT00000033736
# >transcript:GS_16749|||sp|P68657|EA10_ECOL6%%Lambda prophage-derived protein ea10 OS=Escherichia coli O6 GN=ea10 PE=3 SV=1%%4.2e-41
# -> >transcript_GS_16749
rule fasta_clean:
    input:
        trans = "{transcripts}.{suffix}"
    output:
        trans = "{transcripts}.{suffix,(fasta|fa)}.clean"
    run:
        f = open(output[0],'w')
        for line in open(input[0], 'r'):
            if line.startswith(">"):
                prefix = line.strip("\n").split(" ")[0].split("|")[0]
                prefix = re.split('\.\d+$', prefix)[0]
                prefix = prefix.replace(':', '_')
                f.write(prefix + "\n")
            else:
                f.write(line)
        f.close()

# translate reference transcripts, first frame only
# (assuming reference transcripts are already the right ORF)
rule reference_transcript_translation:
    input:
        fasta = lambda wc: TRANSCRIPTS[wc.proteins] + ".clean"
    output:
        "translation/{proteins}.transeq"
    shell:
        "transeq {input} {output} -frame 1 -trim -clean"

# remove transeq frame info (since only 1. frame anyways)
rule reference_transcript_translation_id_fix:
    input:
        fasta = "translation/{proteins}.transeq"
    output:
        fasta = "translation/{proteins}.1frame"
    run:
        with open(output.fasta, 'w') as out:
            with open(input.fasta, 'r') as f:
                for line in f:
                    if line.startswith(">"):
                        out.write(line.strip("\n").split(' ')[0][0:-2] + "\n")
                    else:
                        out.write(line)


### Translate DEG Novel Transcripts ###

# identify and export ORFs for the novel transcripts with TransDecoder
rule novel_transcript_translation:
    input:
        fasta = lambda wc: os.path.abspath(TRANSCRIPTS[wc.proteins]) + ".clean"
    output:
        peps = "transdecoder/{proteins}/{file}.clean.transdecoder.pep"
    params:
        outdir = "transdecoder/{proteins}",
    shell:
        "cd {params.outdir} && " \
        "TransDecoder.LongOrfs -t {input.fasta} && " \
        "TransDecoder.Predict -t {input.fasta}"

rule novel_transcript_translation_seq_fix:
    input:
        peps = lambda wc: "transdecoder/" + wc.proteins + "/" + os.path.basename(TRANSCRIPTS[wc.proteins]) + ".clean.transdecoder.pep"
    output:
        fasta = "translation/{proteins}.pre"
    shell:
        "cp {input.peps} {output.fasta} && " \
        "sed -i 's/\\*//g' {output.fasta}" # remove stop codon "*"

# clean TransDecoder ORFs ids to better represent original transcript ids
# e.g.: Gene.1::TCONS_00002496::g.1::m.1 -> TCONS_00002496_ORF1
rule novel_transcript_translation_id_fix:
    input:
        fasta = "translation/{proteins}.pre"
    output:
        fasta = "translation/{proteins}.orfs"
    run:
        with open(input.fasta, 'r') as fin:
            with open(output.fasta, 'w') as fout:
                last_id = "none"
                counter = 1
                for line in fin:
                    if line.startswith(">"):
                        id = line.split("::")[1]
                        if (id == last_id):
                            counter += 1
                        else:
                            counter = 1
                            last_id = id
                        fout.write(">" + id + "_ORF" + str(counter) + "\n")
                    elif line.strip("\n"): # write non empty lines
                        fout.write(line)


### Translation Type Selection ###

# selection of ORF or 1. Frames for novel or reference transcripts, resp.
# TODO: replace by input function for each relevant rule to prevent fasta copies
rule select_translations:
    input:
        trans = lambda wc: "translation/" + wc.proteins + ".orfs" if NOVELS[wc.proteins] else
                           "translation/" + wc.proteins + ".1frame"
    output:
        fasta = "translation/{proteins}.fasta"
    shell:
        "cp {input.trans} {output.fasta}"
