### Visualize Priority Progress ###

# count # of input transcripts
rule count_input_transcripts:
    input:
        fasta = "translation/{proteins}.fasta"
    output:
        count = "visualization/{proteins}/input.count.txt"
    shell:
        "grep \"^>\" {input.fasta} | wc -l > {output.count}"

# count # of hits per priority
rule count_priority_hits:
    input:
        hits = lambda wc: expand("priority/{proteins}/{db}.found.txt", proteins =  wc.proteins, db = PRIORITIES[wc.proteins])
    output:
        counts = "visualization/{proteins}/found.counts.txt"
    run:
        p_total = 0
        with open(output.counts, 'w') as f_out:
            for prio in PRIORITIES[wildcards.proteins]:
                p_name = prio
                p_file = "priority/" + wildcards.proteins + "/" + prio + ".found.txt"
                p_count = 0
                with open(p_file, 'r') as f_in:
                    p_count = sum(1 for _ in f_in)
                p_total += p_count
                p_line = p_name + "\t" + p_file + "\t" + str(p_count) + "\t" + str(p_total) + "\n"
                print(p_line)
                f_out.write(p_line)

# plot priority progress bars
rule priority_progress_plot:
    input:
        total = "visualization/{proteins}/input.count.txt",
        found = "visualization/{proteins}/found.counts.txt"
    output:
        pdf = "visualization/{proteins}/priority.progress.pdf",
        png = "visualization/{proteins}/priority.progress.png"
    run:
        R(r'''
        library(ggplot2)

        file.total <- "{input.total}"
        file.found <- "{input.found}"
        file.plot1 <- "{output.pdf}"
        file.plot2 <- "{output.png}"

        print(file.total)

        total <- as.integer(readLines(file.total))
        found <- read.table(file.found, sep = "\t", stringsAsFactors = FALSE)
        colnames(found) <- c("Priority", "File", "Hits", "Hits.Added")
        found$Hits.Rest <- total - found$Hits.Added

        anno <- vector(mode = "list", length = nrow(found))
        for (i in 1:nrow(found)){{
          anno[[i]] <- data.frame(
            Priority = c(rep(found$Priority[i], found$Hits.Added[i]), rep(found$Priority[i], found$Hits.Rest[i])),
            Status = c(rep("Annotated", found$Hits.Added[i]), rep("Not Annotated", found$Hits.Rest[i]))
          )
        }}
        anno <- do.call(rbind, anno)
        anno$Status <- relevel(anno$Status, ref = "Not Annotated")

        g <- ggplot(data = anno, mapping = aes(x = Priority, color = Status, fill = Status)) +
          geom_bar(position = "stack", alpha = I(0.5))

        ggsave(file.plot1, g)
        ggsave(file.plot2, g)
        ''')
