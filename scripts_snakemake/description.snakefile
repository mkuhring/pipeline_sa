### Extract Blast Hit Descriptions for Transcript without GO ###

# get transcript without any GO hits
rule blast_description_nogo_remainder:
    input:
        fasta = lambda wc: "priority/" + wc.proteins + "/" + PRIORITIES[wc.proteins][-1] + ".remainder.fasta"
    output:
        ids = "description/{proteins}/missing.txt"
    run:
        with open(input.fasta, 'r') as f_in:
            with open(output.ids, 'w') as f_out:
                for line in f_in:
                    if line.startswith(">"):
                        f_out.write(line[1:])

# extract best blast evalue and description for non-GO-annotated sequences
rule blast_description_extract:
    input:
        blast = "blast/{proteins}/{db}.hits.xml",
        ids = "description/{proteins}/missing.txt"
    output:
        desc = "description/{proteins}/blast.{db}.descr.txt"
    run:
        # non annotated transcripts
        trans = set()
        with open(input.ids, 'r') as f_in:
            for line in f_in:
                trans.add(line.strip("\n").strip())

        # collect best e-value per query
        best_hits = dict()
        best_desc = dict()

        # simply parse all Hsp_evalue per Iteration (aka query sequence) from the Blast xml
        # (xml parsing based on http://stackoverflow.com/a/1912516)
        from xml.dom import minidom
        xmldoc = minidom.parse(input.blast)
        queries = xmldoc.getElementsByTagName('Iteration')

        for q in queries:
            iter_def = q.getElementsByTagName('Iteration_query-def')[0].childNodes[0].nodeValue
            if iter_def in trans:
                hit_list = q.getElementsByTagName('Hit')
                if hit_list:
                    for hit in hit_list:
                        hit_def = hit.getElementsByTagName('Hit_def')[0].childNodes[0].nodeValue
                        evalue_list = hit.getElementsByTagName('Hsp_evalue')
                        if evalue_list:
                            # find best evalue
                            if iter_def not in best_hits:
                                best_hits[iter_def] = float(1)
                            for e in evalue_list:
                                hit_evalue = float(e.childNodes[0].nodeValue)
                                if hit_evalue < best_hits[iter_def]:
                                    best_hits[iter_def] = hit_evalue
                                    best_desc[iter_def] = hit_def

        # export best evalue per annotated hit
        with open(output.desc, 'w') as f_out:
            for id, evalue in best_hits.items():
                f_out.write(id + "\t" + str(evalue) + "\t" + best_desc[id] + "\n")

# merge all best evalues and descrtiptions for all blast-based priorities
rule blast_description_merge:
    input:
        evalues = lambda wc: expand("description/{proteins}/blast.{db}.descr.txt", proteins =  wc.proteins,
                                    db = [prio for prio in PRIORITIES[wc.proteins] if prio not in ["GOref", "InterPro"]])
    output:
        merged = "description/{proteins}/blast.desc.merged.txt"
    shell:
        "cat {input.evalues} > {output.merged}"

# keep only blast hit from earliest priority, i.e. first hit in list per transcript
rule blast_description_reduce:
    input:
        merged = "description/{proteins}/blast.desc.merged.txt"
    output:
        reduced = "description/{proteins}/blast.desc.reduced.txt"
    run:
        R(r'''
            file.merged <- "{input.merged}"
            table.merged <- read.table(file.merged, sep = "\t", header = FALSE, stringsAsFactors = FALSE)
            print(head(table.merged))
            trans.dups <- duplicated(table.merged$V1)
            table.reduced <- table.merged[!trans.dups, ]
            file.reduced <- "{output.reduced}"
            write.table(table.reduced, file.reduced, sep = "\t", quote = FALSE, col.names = FALSE, row.names = FALSE)
        ''')

# get evalues per transcript
rule blast_description_evalues:
    input:
        reduced = "description/{proteins}/blast.desc.reduced.txt"
    output:
        evalues = "description/{proteins}/blast.desc.evalues.txt"
    shell:
        "cut -f 1,2 {input.reduced} > {output.evalues}"

# get description per transcript
rule blast_description_descr:
    input:
        reduced = "description/{proteins}/blast.desc.reduced.txt"
    output:
        descr = "description/{proteins}/blast.desc.extract.txt"
    shell:
        "cut -f 1,3 {input.reduced} > {output.descr}"
