### Blast2GO Execution & Parsing ###

# dummy rule for manual Blast2GO execution
#
# it will break workflow execution since it won't produce the output files
# execute Blast2GO manually and continue the workflow
rule blast2go_manually:
    input:
        "blast/{proteins}/{db}.hits.xml"
    output:
        "blast2go/{proteins}/{db}_hits.annot",
        "blast2go/{proteins}/{db}_hits.mapping"
    run:
        message = '''
### BLAST2GO ###
run the following steps manually in Blast2GO
1. import blast results:
   ''' + input[0] + '''
   * File -> Load -> Load Blast Results -> XML Files (default parameters)
2. run the mapping
   * mapping button "Run Mapping", default parameters
3. run the annotation steps
   * annot button "Run Annotation"
   non-default parameters (derived from blast search):
   * E-Value-Hit-Filter = 1.0E-3
   * Hit Filter = 20
   * Only hits with GOs = True
4. export results as b2g (as backup), annot and mapping file:
   ''' + output[0] + "\n   " + output[1] + '''
   *     b2g: File -> Save As...
   *   annot: File -> Export -> Export Annotations -> Export Annotations (Choose a Format = .annot)
   * mapping: File -> Epxort -> Export Mapping Results (Formats = by Seq)
'''
        print(message)

# correct UniProt descriptions of Blast2GO annotations
#
# Blast2GO joins UniProt ids and descriptions while importing from Blast xml
# (seems necessary for the Mapping step, so don't turn it of while importing!),
# but thereby corrupt the description in the annot and mapping output, as shown here:
#
# fasta:   >tr|U1P1W7|U1P1W7_ASCSU Isocitrate dehydrogenase subunit gamma OS=Ascaris suum GN=ASU_02167 PE=4 SV=1
# blast:   <Hit_id>tr|U1P1W7|U1P1W7_ASCSU</Hit_id>
#          <Hit_def>Isocitrate dehydrogenase subunit gamma OS=Ascaris suum GN=ASU_02167 PE=4 SV=1</Hit_def>
# annot:   U1P1W7_ASCSUIsocitrate dehydrogenase subunit gamma OS=Ascaris suum GN=ASU_02167 PE=4 SV=1
# mapping: tr|U1P1W7|U1P1W7_ASCSUIsocitrate dehydrogenase subunit gamma OS=Ascaris suum GN=ASU_02167 PE=4 SV=1
#
# Also, Blast2GO seems to remove words from the descriptions, e.g.: (-like) protein, putative, homolog.
#
# Therefore, UniProt ids, entry names and descriptions are collected from the Blast xml (<Hit_id>, <Hit_def>)
# and descriptions are replaced whenever the ids resp. entry names match as prefix.
#
# Side note: Blast xml results based on NCBI databases seem to be not effected by this.
# But since they are not identified and handled separately within this pipeline, they
# are processed by this rule too. However, this shouldn't have any effect.
#
# TODO: Check from time to time if newer Blast2GO versions fix this issues.
rule blast2go_uniprot_cleanup_annot:
    input:
        blast = "blast/{proteins}/{db}.hits.xml",
        annot = "blast2go/{proteins}/{db}_hits.annot"
    output:
        annot = "blast2go/{proteins}/{db}_hits.annot.clean"
    run:
        # collect UniProt ids
        ids_annot = dict()

        # simply parse all Hit_id per Iteration (aka query sequence) from the Blast xml
        # (xml parsing based on http://stackoverflow.com/a/1912516)
        from xml.dom import minidom
        xmldoc = minidom.parse(input.blast)

        queries = xmldoc.getElementsByTagName('Iteration')
        for q in queries:
            iter_def = q.getElementsByTagName('Iteration_query-def')[0].childNodes[0].nodeValue
            hit_list = q.getElementsByTagName('Hit')
            if hit_list:
                ids_annot[iter_def] = dict()
                for s in hit_list:
                    hit_id = s.getElementsByTagName('Hit_id')[0].childNodes[0].nodeValue
                    hit_def = s.getElementsByTagName('Hit_def')[0].childNodes[0].nodeValue
                    splits = hit_id.split('|')
                    if splits[0] in ["sp", "tr"]: # check if UniProt hit
                        ids_annot[iter_def][hit_id.split('|')[-1]] = hit_def

        # iterate annot file
        with open(input.annot, 'r') as f_in:
            with open(output.annot, 'w') as f_out:
                for line in f_in:
                    splits = line.split("\t")
                    if len(splits) == 3:
                        seq_id = splits[0]
                        for id in ids_annot[seq_id]:
                            if splits[2].startswith(id):
                                splits[2] = ids_annot[seq_id][id] + "\n"
                                break
                        f_out.write("\t".join(splits))
                    else:
                        f_out.write(line)
