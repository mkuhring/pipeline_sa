### GFF(3)/GTF renaming, conversion and filtering ###

# symlink gff3 to gff
rule gff3_to_gff:
    input:
        "{annotation}.gff3"
    output:
        "{annotation}.gff"
    shell:
        "cp {input} {output}"

# convert gff to gtf (some tools prefer/need gtf, e.g. QualiMap)
rule gff_to_gtf:
    input:
        "{annotation}.gff"
    output:
        temp("{annotation}.gtf.convert")
    log:
        "logs/gffread/{annotation}.gtf.log"
    benchmark:
        "logs/gffread/{annotation}.txt"
    shell:
        "gffread {input} -T -o {output} 2>&1 | tee {log}"

# filter/remove gtf elements with identical start and stop
# TODO: report removed elements (at least the number)
rule gtf_filter_start_stop:
    input:
        "{annotation}.gtf.convert"
    output:
        temp("{annotation}.gtf.filter1")
    log:
        "logs/gtf_filter/{annotation}.gtf.ss.log"
    shell:
        "awk '{{ if ($4 != $5) print $0}}' {input} > {output}; awk '{{ if ($4 == $5) print $0}}' {input} > {log}"

# filter/remove gtf elements with missing gene_id
# TODO: report removed elements (at least the number)
rule gtf_filter_gene_id:
    input:
        "{annotation}.gtf.filter1"
    output:
        temp("{annotation}.gtf.filter2")
    log:
        "logs/gtf_filter/{annotation}.gtf.id.log"
    shell:
        "grep \"gene_id\" {input} > {output}; grep -v \"gene_id\" {input} > {log} || true"

# provide converted or converted+filtered gtf according to GTF_GILTER
rule gtf_tmp_rename:
    input:
        lambda wc: wc.annotation + ".gtf.filter2" if GTF_FILTER else wc.annotation + ".gtf.convert"
    output:
        "{annotation}.gtf"
    shell:
        "mv {input} {output}"


### ID Mappings ###

# extract transcript_id, gene_id and gene_name (if available) from reference GTF file
rule gtf_id_extraction:
    input:
        "{genome}.gtf"
    output:
        "{genome}.gtf.ids"
    shell:
        "{WORKFLOW_PATH}/scripts_python/gtfIDExtraction.py < {input} > {output}"

# clean ids, i.e.
# * replace ":" in ids by "_"
rule gtf_id_clean:
    input:
        ids = "{genome}.gtf.ids"
    output:
        ids = "{genome}.gtf.ids.clean"
    run:
        with open(input.ids, 'r') as f_in:
            with open(output.ids,'w') as f_out:
                for line in f_in:
                    f_out.write(line.replace(':', '_'))