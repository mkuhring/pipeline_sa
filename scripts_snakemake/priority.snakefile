### Priority Annotation Control ###

# 1. collect GO annotations, mainly from Blast2GO, optionally from InterPro or if provided from reference GOs
def select_annotations(wc):
    if wc.db == "GOref":
        annot = "priority/" + wc.proteins + "/GOref.mapped.annot"
    elif wc.db == "InterPro":
        annot = "interpro/" + wc.proteins + "/InterPro.priority.annot"
    else:
        annot = "blast2go/" + wc.proteins + "/" + wc.db + "_hits.annot.clean"
    return(annot)

rule priority_collect_GO_annotation:
    input:
        select_annotations
    output:
        "priority/{proteins}/{db}_hits.annot"
    shell:
        "cp {input} {output}"

# 2. identify transcript translations with GO annotation
rule priority_translations_with_GO:
    input:
        "priority/{proteins}/{db}_hits.annot"
    output:
        "priority/{proteins}/{db}.found.txt"
    shell:
        "cat {input} | cut -f1 | sort | uniq > {output}"

# 3. extract transcript translations without GO annotations
# (using faSomeRecords from Kent tools: http://hgdownload.cse.ucsc.edu/admin/exe/linux.x86_64/)
rule priority_translation_remainders:
    input:
        ids = "priority/{proteins}/{db}.found.txt",
        fasta = "priority/{proteins}/{db}.input.fasta"
    output:
        fasta = "priority/{proteins}/{db}.remainder.fasta"
    shell:
        "faSomeRecords -exclude {input.fasta} {input.ids} {output.fasta}"

# 4. indicate fasta input for the next priority iteration
# i.e. reversely request results from previous priority
# select either no-GO translations from previous priority or reference GO filtered resp. full first database
def remainder_selection(wc):
    if PRIORITIES[wc.proteins].index(wc.priority) > 0:
        fasta = "priority/" + wc.proteins + "/" + PRIORITIES[wc.proteins][PRIORITIES[wc.proteins].index(wc.priority)-1] + ".remainder.fasta"
    else:
        fasta = "translation/" + wc.proteins + ".fasta"
    return(fasta)

# TODO: provide input function directly to relevant rules to prevent fasta copies
rule priority_remainder_selection:
    input:
        fasta = remainder_selection
    output:
        fasta = "priority/{proteins}/{priority}.input.fasta"
    shell:
        "cp {input.fasta} {output.fasta}"


### Final Annotation Collection ###

# TODO: ORF reduction/selection
# select ORF with highest number of GOs?
# (means annotation is more specific/detailed in terms of function, i.e. lower GO level)

# merge GO annotations from all priority iterations
rule priority_result_collection:
    input:
        annots = lambda wc: expand("priority/" + wc.sample + "/{priority}_hits.annot", priority=PRIORITIES[wc.sample])
    output:
        annot = "priority/{sample}/complete.annot"
    shell:
        "cat {input.annots} > {output.annot}"



