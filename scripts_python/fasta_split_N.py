#! /usr/bin/python3

# split fasta file into N files

import math

# count input fasta entries
with open(snakemake.input["fasta"], 'r') as f_in:
    fa_count = sum([1 for line in f_in if line.startswith(">")])

# calc number per split
num_splits = snakemake.params["splits"]
num_max = math.ceil(fa_count / num_splits)

# iterate input file
with open(snakemake.input["fasta"], 'r') as f_in:
    counter = 0
    out_idx = 0

    # open first output file
    f_out = open(snakemake.output["fastas"][out_idx], 'w')
    out_idx += 1

    for line in f_in:
        if line.startswith(">"):
            counter += 1
        if counter > num_max:
            # close last output file
            f_out.close()
            # open next output file
            f_out = open(snakemake.output["fastas"][out_idx], 'w')
            out_idx += 1
            counter = 1
        f_out.write(line)
    f_out.close()
